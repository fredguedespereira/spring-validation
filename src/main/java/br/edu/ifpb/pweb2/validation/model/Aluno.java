package br.edu.ifpb.pweb2.validation.model;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.Digits;
import javax.validation.constraints.Email;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;

import org.springframework.format.annotation.DateTimeFormat;

import br.edu.ifpb.pweb2.validation.validator.CodCursoValid;

@Entity
public class Aluno {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="nu_id")
	private Integer id;
	
	@NotBlank(message="Campo obrigatório!")
	@Column(name="nm_nome", length=50)
	private String nome;
	
	@NotBlank(message="Campo obrigatório!")
	@Digits(integer = 6, fraction = 0, message = "Informe um número de até 6 dígitos!")
	@Column(name="nu_matricula", length=9)
	private String matricula;
	
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	@Past(message="Data deve ser passada!")
	@NotNull(message="Campo obrigatório!")
	@Column(name="dt_nascimento")
	private LocalDate dataNascimento;
	
	@NotBlank(message="Campo obrigatório!")
	@Email(message="Informe um email válido!")
	@Column(name="nm_email", length=30)
	private String email;
	
	@CodCursoValid
	@Column(name="cd_curso")
	private String codCurso;
	
	@Min(value=123)
	@Column(name="nu_rg")
	private Integer rg;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getMatricula() {
		return matricula;
	}

	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}

	public LocalDate getDataNascimento() {
		return dataNascimento;
	}

	public void setDataNascimento(LocalDate dataNascimento) {
		this.dataNascimento = dataNascimento;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getCodCurso() {
		return codCurso;
	}

	public void setCodCurso(String codCurso) {
		this.codCurso = codCurso;
	}

	public Integer getRg() {
		return rg;
	}

	public void setRg(Integer rg) {
		this.rg = rg;
	}
	
	

}